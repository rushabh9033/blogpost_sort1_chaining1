# **How does high order functions chaining works?**

## What is High order functions?

A higher-order function is a function that takes a function as an argument or returns a function. A higher-order function is in contrast to first-order functions, which don’t take a function as an argument or return a function as output. Higher-order functions provide the reusability benefit. There are different types of higher-order functions like map, filter, reduce, for-each, flat, etc.

## Map function

The JavaScript map function is applied when it is necessary to call a function for every element of an array. From the generated results, the method will create a new array. Map functions take as callback functions as arguments. The syntax of the javascript map functions is below.

```javascript
array.map(function(element, index, arr), thisvalue)
```

In syntax, the first parameter is the value of the current array element. Index and arr is an optional parameter. There is an example described below.

```javascript
let arr = [3, 4, 5, 7, 8];
let newArr = arr.map((element) => element * 2);
console.log(newArr);
```

```
Output:

[ 6, 8, 10, 14, 16 ]
```

In this example, callback functions are called on every element of an array and double the value then return it to a new array.

## Filter functions

This is another higher-order function which is the filter. The filter array method creates a new array with elements that fall under given criteria from an existing array. The syntax of filter functions describes below.

```javascript
array.filter(function (element, index, arr) {});
```

Filter functions take a callback function as an argument and this is called on every array element.

```javascript
const arr = [5, 10, 25, 55, 36, 40];
const filterArr = arr.filter((element) => element > 10);

console.log(filterArr);
```

```
Output:

[25, 55, 36, 40]
```

## Reduce functions

This reduce function is a method of array class that you can call on any arrays that you have in your JavaScript code. The method will execute a reducer function that you need to provide as the first argument. This method is used for the sum of all array elements. The syntax of reduce functions is given below.

```javascript
array.reduce(function (previousVal, currentVal) {}, 0);
```

Here reduce functions take callback functions as arguments which also takes two arguments previous value and current value. This callback function is called on every array element.

```javascript
const arr = [5, 2, 3, 4, 8, 9, 10];
const total = arr.reduce(
  (previousVal, currentVal) => previousVal + currentVal,
  1
);
console.log(total);
```

```
Output:

42
```

In the above example, if you do not provide the previous value, it takes 0 by default previous value.

If you want to find the array sum which value is double its original value and less than the given condition. Here it's come hand function chaining.

## Function chaining

This concept is useful for combining all higher-order functions in one given array object. Using this concept, every time return when you called different functions and it's the return value that value you used for another function. There is below example shows how functions chaining works.

```javascript
// Example on an array
// Sum the element which double its value and less than and equal to 10
let arr = [3, 4, 5, 7, 8];

let sumOfArr = arr
  .map((element) => element * 2)
  .filter((element) => element <= 10)
  .reduce((accumlator, currentVal) => accumlator + currentVal, 0);

console.log(sumOfArr);
```

```
Output:

24
```

There is another example that describes how to work chaining an array of object.

```javascript
// Console fullname which age is less than 30
let user = [
  { fname: "Rajesh", age: 30, lname: "Patel" },
  { fname: "Suresh", age: 30, lname: "Shah" },
  { fname: "Rakesh", age: 47, lname: "Mehta" },
  { fname: "Arjun", age: 28, lname: "Bhatt" },
];

const fullName = user
  .filter((element) => element.age <= 30)
  .map((element) => `${element.fname} ${element.lname}`);

console.log(fullName);
```

```
Output:

[ 'Rajesh Patel', 'Suresh Shah', 'Arjun Bhatt' ]
```
